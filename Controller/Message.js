const Joi = require("joi");
const Guardians = require("../Model/Guardians");
const user = require("../Model/user");
const Issue = require("../Model/Issues");

var allguardians = []

async function SMStoGuardians(req) {
    var Guardianslist = await Guardians.find({ user_id: req.headers['userid'] }, {})
    for (let i = 0; i < Guardians.length; i++) {
        let issue_id = `${Date.now()}${Guardianslist[i].guardian_id}`
        let respondingurl = `${req.get('host')}/RespondToEmergency/issue_id=${issue_id}`
        // await Helper.SMS({
        //     to: req.body[i].mobile,
        //     message: message ? message : `${Guardians[i].name} please save I'm in danger please track my location ${req.body.location} and please click on this link '' to respond this message`
        // })
        allguardians.push({
            issue_id: issue_id,
            user_id: req.headers['userid'],
            guardian_id: Guardianslist[i].guardian_id,
            message: req.body.message ? `${req.body.message} please track my location ${req.body.location} and please click on this link ${respondingurl} to respond to this message` : `${Guardianslist[i].name} please save I'm in danger, please track my location ${req.body.location} and please click on this link ${respondingurl} to respond to this message`,
        })
    }
}
async function SMStoVolenteers(req) {
    // const victimuser = await user.find({ user_id: req.headers['userid'] }, {})
    var userslist = []
    var guardianslistarr = []
    var guardiansfoundlist = [req.headers['userid']]
    var radious = 0
    do {
        radious += 5
        if (radious > 40) break;
        if (guardianslistarr.length != 0) {
            for (let i = 0; i < guardianslistarr.length; i++) {
                guardiansfoundlist.push(guardianslistarr[i].user_id)
            }
        }
        userslist = await user.find({
            location:
            {
                $geoWithin:
                    { $centerSphere: [[req.body.location[0], req.body.location[1]], radious / 6378.1] }
            },
            user_id: {
                $nin: guardiansfoundlist
            }
        }, {})
        if (userslist.length > 0) {
            for (let j = 0; j < userslist.length; j++) {
                guardianslistarr.push(userslist[j])
            }
        }
    }
    while (guardianslistarr.length < 5)

    if (guardianslistarr.length > 0) {
        for (let i = 0; i < guardianslistarr.length; i++) {
            let issue_id = `${Date.now()}${guardianslistarr[i].user_id}`

            let respondingurl = `${req.get('host')}/RespondToEmergency/issue_id=${issue_id}`
            // await Helper.SMS({
            //     to: req.body[i].mobile,
            //     message: message ? message : `${Guardians[i].name} please save I'm in danger please track my location ${req.body.location} and please click on this link '' to respond this message`
            // })
            allguardians.push({
                issue_id: issue_id,
                user_id: req.headers['userid'],
                guardian_id: guardianslistarr[i].user_id,
                message: req.body.message ? `${guardianslistarr[i].name}, ${message} please track my location ${req.body.location} and please click on this link ${respondingurl} to respond to this message` : `${guardianslistarr[i].name}, please save I'm in danger please track my location ${req.body.location} and please click on this link ${respondingurl} to respond to this message`,
            })
        }
    }
    // console.log(userslist)
}

module.exports = {
    EmergencyTrigger: async (req, res) => {
        await SMStoGuardians(req)
        // console.log(allguardians)
        await SMStoVolenteers(req)
        // console.log(allguardians)
        await Issue.insertMany(allguardians)
        res.status(200).json({
            status: 200,
            message: "Messages triggered"
        })
    },
    RespondToEmergency: async (req, res) => {
        var issue_id = req.query.issue_id ? req.query.issue_id : ""
        if (issue_id == "") res.status(400).json({
            status: 400,
            message: "Invalid request"
        })
        else {
            let userid = await Issue.find({ issue_id: issue_id }, { user_id: 1, _id: 0 }).limit(1)
            // console.log(userid)
            let userdata = await user.find({ user_id: Number(userid[0].user_id) }, { mobile: 1, name: 1, _id: 0 })
            // console.log(userid,userdata)
            // await Helper.SMS({
            //     to: userdata.mobile,
            //     message: `${userdata.name} is Responded to your message`
            // })
            await Issue.updateOne({ issue_id: issue_id }, {
                $set: {
                    status: "Responded"
                }
            })
            res.status(200).json({
                status: 200,
                message: "Responded"
            })
        }
    }
}