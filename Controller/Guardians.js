const user = require('../Model/user')
const Joi = require('joi')
const Helper = require('../Helper/Helper')
const Guardians = require('../Model/Guardians')

module.exports = {
    AddGuardians: async (req, res) => {
        const joischema = Joi.object({
            name: Joi.string().required(),
            mobile: Joi.string().required(),
            relation: Joi.string().required(),
            priority: Joi.number().required()
        })
        if (req.body.length == 5) {
            // console.log(req.body.length)
            var error = ""
            for (let i = 1; i <= req.body.length; i++) {
                let validationresult = joischema.validate(req.body[i])
                if (validationresult.error) {
                    error = `${error} ${validationresult.error.details[0]['message']} in ${i}`
                }
            }
            if (error) {
                res.status(400).json({
                    status: 400,
                    message: error
                })
            } else {
                let arr_details = await Helper.Findarraydetails(req.body, 'mobile')
                if (arr_details.duplicates.length > 0) {
                    res.status(400).json({
                        status_code: 400,
                        message: `${arr_details.duplicates.join(',')} are repeating`
                    })
                }
                else {
                    for (let i = 0; i < req.body.length; i++) {
                        req.body[i].user_id = req.headers['userid']
                        req.body[i].guardian_id = i
                    }
                    let userdata = await user.find({ user_id: req.headers['userid'] }, {})
                    await Guardians.insertMany(req.body)
                    // for (let i = 0; i < req.body.length; i++) {
                    //     await Helper.SMS({
                    //         to: req.body[i].mobile,
                    //         message: `Hi ${req.body[i].name} I'm requesting you to be as as my Guardian because a I trust you,
                    //Please be with me `${req.get('host')}/RequestAccept/${}`
                    // please click on this  to accept request
                    //          Thank you`//`Hi ${req.body[i].name} I'm ${userdata[0].name} please save me I'm in danger track me with http://google.com and please click on this link so i can get to know Your are coming`
                    //     })
                    // let responseurl = `${req.get('host')}/AcceptRequest?user=${req.headers['userid']}_${req.body[i].guardian_id}`
                    // }
                    res.status(200).json({
                        status: 200,
                        message: "Guardians are added"
                    })
                }
            }
        } else {
            res.status(400).json({
                status_code: 400,
                message: "Please add 5 guardians"
            })
        }
    },
    UpdateGuardians: async (req, res) => {
        const joischema = Joi.object({
            name: Joi.string().required(),
            mobile: Joi.string().required(),
            relation: Joi.string().required(),
            priority: Joi.number().required(),
            guardian_id: Joi.number().required(),
        })
        if (req.body.length == 5) {
            // console.log(req.body.length)
            var error = ""
            for (let i = 1; i <= req.body.length; i++) {
                let validationresult = joischema.validate(req.body[i])
                if (validationresult.error) {
                    error = `${error} ${validationresult.error.details[0]['message']} in ${i}`
                }
            }
            if (error) {
                res.status(400).json({
                    status: 400,
                    message: error
                })
            } else {
                let arr_details = await Helper.Findarraydetails(req.body, 'mobile')
                if (arr_details.duplicates.length > 0) {
                    res.status(400).json({
                        status_code: 400,
                        message: `${arr_details.duplicates.join(',')} are repeating`
                    })
                }
                else {
                    for (let i = 0; i < req.body.length; i++) {
                        await Guardians.updateOne({ user_id: req.headers['userid'], guardian_id: req.body[i].guardian_id }, {
                            $set: {
                                name: req.body[i].name,
                                mobile: req.body[i].mobile,
                                relation: req.body[i].relation,
                                priority: req.body[i].priority
                            }
                        })
                    }
                    // let userdata = await user.find({ user_id: req.headers['userid'] }, {})
                    // await Guardians.insertMany(req.body)
                    // for (let i = 0; i < req.body.length; i++) {
                    //     await Helper.SMS({
                    //         to: req.body[i].mobile,
                    //         message: `Hi ${req.body[i].name} I have added as you as my Guardian because a I trust you,Please be with me
                    //          Thank you`//`Hi ${req.body[i].name} I'm ${userdata[0].name} please save me I'm in danger track me with http://google.com and please click on this link so i can get to know Your are coming`
                    //     })
                    // }
                    res.status(200).json({
                        status: 200,
                        message: "Guardians are Updated"
                    })
                }
            }
        } else {
            res.status(400).json({
                status_code: 400,
                message: "Please add 5 guardians"
            })
        }
    },
    AcceptRequest: async (req, res) => {
        var userreq = req.query.user ? req.query.user : "";
        if (userreq == "") {
            res.status(400).json({
                status: 400,
                message: "User data missing"
            })
        }
        else {
            let userguardianid = userreq.split('_')
            // console.log(userguardianid[0], userguardianid[1])
            await Guardians.updateOne({ user_id: userguardianid[0], guardian_id: userguardianid[1] }, {
                requeststatus: "Accepted"
            })
            // let userdata = await user.find({ user_id: userguardianid[0] }, {})
            let guardiandata = await Guardians.find({ user_id: userguardianid[0], guardian_id: userguardianid[1] }, {})
            if (guardiandata.length > 0) {
                // await Helper.SMS({
                //     to: req.body[i].mobile,
                //     message: `${guardiandata[0].name} accepted your request to be as your guardian`
                // })
                res.status(200).json({
                    status: 200,
                    message: "Requested accepted"
                })
            }
            else {
                res.status(200).json({
                    status: 200,
                    message: "Request not valid"
                })
            }
        }
    }
}