const user = require('../Model/user')
const Joi = require('joi')
const md5 = require('md5');
const nodemailer = require('nodemailer');
const Helper = require('../Helper/Helper');
// import hmacSHA512 from 'crypto-js/hmac-sha512';
// import Base64 from 'crypto-js/enc-base64';

module.exports = {
    RegisterUser: async (req, res) => {
        // console.log(fields.mobile)
        // let mobile = fields.mobile
        let joischema = Joi.object({
            mobile: Joi.number().required(),
            location: Joi.array().required(),
        })
        const validationresult = joischema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response1 = {
                'message': message,
                'status_code': 400
            }
            res.status(400).json(response1)
        } else {
            try {
                let getuserid = await user.find({}, { user_id: 1, _id: 0 }).sort({ user_id: -1 }).limit(1)
                // console.log(getuserid)
                let otp = await Math.floor(Math.random() * (9999 - 1000)) + 1000;
                let userid = 1000
                if (getuserid.length > 0) {
                    userid = getuserid[0].user_id + 1
                }
                await user({
                    user_id: userid,
                    mobile: req.body.mobile,
                    tempmobile: 0,
                    location: req.body.location,
                    otp: otp,
                }).save()
                // await Helper.Email({
                //     tomail: 'divakarparuchuri123@gmail.com',
                //     subject: 'Guardian mobile verification',
                //     text: '',
                //     html: `<div style='text-align:center'>
                // <h3><p>Please use this OTP </p><p style='color:blue;font-weight:bold'> ${otp} to verify your mobile number </p></h3>
                // <h5>Your safety is our priority</h5>
                // </div>`
                // })
                res.status(400).json({
                    status_code: 400,
                    message: `Otp sent to ${req.body.mobile}`,
                    otp: otp
                })
            }
            catch (error) {
                console.log(error)
                res.status(400).json({
                    status_code: 400,
                    message: "Mobile number already existed"
                })
            }
        }
    },
    VerifyOtp: async (req, res) => {
        var joischema = Joi.object({
            mobile: Joi.number().required(),
            otp: Joi.number().required()
        })
        let validationresult = joischema.validate(req.body)
        if (validationresult.error) res.status(400).json({
            'message': validationresult.error.details[0]['message'],
            'status_code': 400
        })
        else {
            let verified = await user.find({ mobile: req.body.mobile, otp: req.body.otp }, {})
            if (verified.length > 0) {
                await user.updateOne({ mobile: req.body.mobile, otp: req.body.otp }, {
                    $set: {
                        otp: '',
                        status: 'Verified'
                    }
                })
                res.status(200).json({
                    status_code: 200,
                    message: "Otp verified successfully"
                })
            }
            else {
                res.status(400).json({
                    status: 400,
                    message: "Invalid Otp"
                })
            }
        }
    },
    ProfileUpdate: async (req, res) => {
        const validationschema = Joi.object({
            name: Joi.string().optional(),
            lat: Joi.number().optional(),
            long: Joi.number().optional(),
            password: Joi.string().optional(),
            DOB: Joi.string().optional(),
            profession: Joi.string().optional(),
            is_volenter_also: Joi.string().optional()
        })
        const validationresult = validationschema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response1 = {
                'message': message,
                'status_code': 400
            }
            res.status(400).json(response1)
        } else {
            try {
                if (req.body.password ? true : false) {
                    req.body.password = md5(req.body.password)
                    await user.updateOne({ user_id: req.headers['userid'] }, req.body)
                    res.status(200).json({
                        status_code: 200,
                        message: "Profile updated successfully"
                    })
                }
            }
            catch (err) {
                res.status(500).json({
                    status_code: 500,
                    message: "Unable to update please try again"
                })
            }
        }
    },
    Login: async (req, res) => {
        let validationschema = Joi.object({
            mobile: Joi.number().required(),
            password: Joi.string().required()
        })
        const validationresult = validationschema.validate(req.body)
        if (validationresult.error) {
            res.status(400).json({
                'message': validationresult.error.details[0]['message'],
                'status_code': 400
            })
        } else {
            let userdata = await user.find({ mobile: req.body.mobile, password: md5(req.body.password) }, {})
            if (userdata.length > 0) {
                let token = md5(new Date())
                await user.updateOne({ mobile: req.body.mobile }, {
                    token: token
                })
                res.status(200).json({
                    status_code: 200,
                    message: "User Logged in successfully",
                    userdata: {
                        userid: userdata[0].user_id,
                        token: token
                    }
                })
            }
            else {
                res.status(400).json(
                    {
                        status_code: 400,
                        message: "Invalid credentials"
                    }
                )
            }
        }
    },
    UpdateMobile: async (req, res) => {
        let joischema = Joi.object({
            mobile: Joi.number().required()
        })
        let validationresult = joischema.validate(req.body)
        if (validationresult.error) res.status(400).json({
            status_code: 400,
            message: validationresult.error.details[0]['message']
        })
        else {
            let matched = await user.find({ $or: [{ mobile: req.body.mobile }, { tempmobile: req.body.mobile }] }, {})
            if (matched.length > 0) res.status(400).json({
                status_code: 400,
                message: "This number already registered"
            })
            else {
                let otp = await Math.floor(Math.random() * (9999 - 1000)) + 1000;
                // await Helper.Email({
                //     tomail: 'divakarparuchuri123@gmail.com',
                //     subject: 'Guardian mobile verification',
                //     text: '',
                //     html: `<div style='text-align:center'>
                // <h3><p>Please use this OTP </p><p style='color:blue;font-weight:bold'> ${otp} to verify your mobile number </p></h3>
                // <h5>Your safety is our priority</h5>
                // </div>`
                // })
                await user.updateOne({ user_id: req.headers['userid'] }, {
                    $set: {
                        tempmobile: req.body.mobile,
                        otp: otp
                    }
                })
                res.status(200).json({
                    status_code: 200,
                    message: 'Otp sent to ' + req.body.mobile,
                    otp: otp
                })
            }
        }
    },
    VerifyMobileUpdate: async (req, res) => {
        var joischema = Joi.object({
            mobile: Joi.number().required(),
            otp: Joi.number().required()
        })
        let validationresult = joischema.validate(req.body)
        if (validationresult.error) res.status(400).json({
            'message': validationresult.error.details[0]['message'],
            'status_code': 400
        })
        else {
            let verified = await user.find({ tempmobile: req.body.mobile, otp: req.body.otp }, {})
            if (verified.length > 0) {
                try {
                    await user.updateOne({ tempmobile: req.body.mobile, otp: req.body.otp }, {
                        $set: {
                            mobile: req.body.mobile,
                            tempmobile: '',
                            otp: ''
                        }
                    })
                    res.status(200).json({
                        status_code: 200,
                        message: "Otp verified successfully"
                    })
                }
                catch (err) {
                    res.status(200).json({
                        status_code: 400,
                        message: "Mobile number already registered"
                    })
                }
            }
            else {
                res.status(400).json({
                    status: 400,
                    message: "Invalid Otp"
                })
            }
        }
    },
    ForgotPassword: async (req, res) => {
        let joischema = Joi.object({
            mobile: Joi.number().required()
        })
        let validationresult = joischema.validate(req.body)
        if (validationresult.error) res.status(400).json({
            status_code: 400,
            message: validationresult.error.details[0]['message']
        })
        else {
            let matched = await user.find({ mobile: req.body.mobile }, {})
            if (matched.length > 0) {
                let otp = await Math.floor(Math.random() * (9999 - 1000)) + 1000;
                // await Helper.Email({
                //     tomail: 'divakarparuchuri123@gmail.com',
                //     subject: 'Guardian mobile verification',
                //     text: '',
                //     html: `<div style='text-align:center'>
                // <h3><p>Please use this OTP </p><p style='color:blue;font-weight:bold'> ${otp} to verify your mobile number </p></h3>
                // <h5>Your safety is our priority</h5>
                // </div>`
                // })
                await user.updateOne({ mobile: req.body.mobile }, {
                    $set: {
                        otp: otp
                    }
                })
                res.status(200).json({
                    status_code: 200,
                    message: "Otp sent to " + req.body.mobile,
                    otp: otp
                })
            }
            else {
                res.status(400).json({
                    status_code: 400,
                    message: "Mobile number not registered"
                })
            }
        }
    },
    VerifyforgotPassword: async (req, res) => {
        var joischema = Joi.object({
            mobile: Joi.number().required(),
            password: Joi.string().required(),
            otp: Joi.number().required()
        })
        let validationresult = joischema.validate(req.body)
        if (validationresult.error) res.status(400).json({
            'message': validationresult.error.details[0]['message'],
            'status_code': 400
        })
        else {
            let verified = await user.find({ mobile: req.body.mobile, otp: req.body.otp }, {})
            if (verified.length > 0) {
                await user.updateOne({ mobile: req.body.mobile, otp: req.body.otp }, {
                    $set: {
                        password: md5(req.body.password),
                        otp: ''
                    }
                })
                res.status(200).json({
                    status_code: 200,
                    message: "Password updated successfully"
                })
            }
            else {
                res.status(400).json({
                    status: 400,
                    message: "Invalid Otp"
                })
            }
        }
    }
}