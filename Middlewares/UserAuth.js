const users = require('../Model/user')
module.exports = {
    Auth: async (req, res, next) => {
        if ((typeof req.headers['userid'] == undefined || req.headers['userid'] == null) || (typeof req.headers['token'] == undefined || req.headers['token'] == null)) {
            res.status(400).send({
                message: "User id or token is missing"
            })
        }
        else {
            const user_id = req.headers['userid']
            const token = req.headers['token']
            var userdetails = await users.find({ user_id: user_id, token: token }, {})
            if (userdetails.length > 0) {
                next();
            }
            else {
                res.status(400).json({
                    status_code: 400,
                    message: "Invalid user credentials"
                })
            }
        }
    }
}