const Route = require('express').Router()
const users = require('../Controller/User')
const Guardians = require('../Controller/Guardians')
const UserAuth = require('../Middlewares/UserAuth')
const Message = require('../Controller/Message')

//----------User Module-------------//
Route.post('/Register', users.RegisterUser)
Route.put('/ProfileUpdate', UserAuth.Auth, users.ProfileUpdate)
Route.put('/VerifyOtp', users.VerifyOtp)
Route.put('/UpdateMobile', UserAuth.Auth, users.UpdateMobile)
Route.put('/VerifyMobileUpdateOtp', UserAuth.Auth, users.VerifyMobileUpdate)
Route.put('/ForgotPassword', users.ForgotPassword)
Route.put('/VerifyforgotPasswordOtp', users.VerifyforgotPassword)
//----------End User Module-------------//

//----------Guardians-------------------//
Route.post('/AddGuardians', UserAuth.Auth, Guardians.AddGuardians)
Route.put('/UpdateGuardians', UserAuth.Auth, Guardians.UpdateGuardians)
Route.post('/AcceptRequest', Guardians.AcceptRequest)
//----------End Guardians-------------------//

//----------Emergency SMS-----------------------------//
// Route.get('/SMStoGuardians', Message.SMStoGuardians)
Route.post('/EmergencyTrigger', Message.EmergencyTrigger)
Route.get('/RespondToEmergency', Message.RespondToEmergency)
//----------Emergency SMS END-----------------------------//
Route.put('/Login', users.Login)

module.exports = Route