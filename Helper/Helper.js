const nodemailer = require('nodemailer');

module.exports = {
    Email: async (params) => {
        var transporter = nodemailer.createTransport({
            // service: process.env.EmailService,
            // auth: {
            //     user: process.env.MailerEmail,
            //     pass: process.env.EmailPassword
            // }
            host: process.env.EmailService,
            port: 537,
            secure: true, // use SSL
            auth: {
                user: process.env.MailerEmail,
                pass: process.env.EmailPassword
            }
        });
        var mailOptions = {}
        if (params.html != "") {
            mailOptions = {
                from: process.env.MailerEmail,
                to: params.tomail,
                subject: params.subject,
                html: params.html//'<h1>Welcome</h1><p>That was easy!</p>'
            }
        }
        else if (params.text != "") {
            mailOptions = {
                from: process.env.MailerEmail,
                to: params.tomail,
                subject: params.subject,
                text: params.text//'<h1>Welcome</h1><p>That was easy!</p>'
            }
        }
        await transporter.sendMail(mailOptions, (error, info) => {
            if (error) { console.log(error); return true }
            else { console.log(`Email sent to : ${info.response}`); return true }
        })
    },
    SMS: async (params) => {
        const accountSid = process.env.twiliosid;
        const authToken = process.env.twiliotoken;
        const client = require('twilio')(accountSid, authToken);
        client.messages
            .create({
                to: params.to,
                from: process.env.twiliophonenumber,
                body: params.message,
            })
            .then(message => {
                console.log(message.sid)
                return message.sid
            });
    },
    Findarraydetails: async (array, filterkey) => {
        let uniques = []
        let duplicates = []
        let totdetails = {}
        for (let i = 0; i < array.length; i++) {
            if (!uniques.includes(array[i][filterkey])) {
                uniques.push(array[i][filterkey])
                totdetails[array[i][filterkey]] = 0
            }
            else {
                duplicates.push(array[i][filterkey])
                totdetails[array[i][filterkey]] = totdetails[array[i][filterkey]] + 1
            }
        }
        return {
            uniques: uniques,
            duplicates: duplicates,
            totdetails: totdetails
        }
    }
}