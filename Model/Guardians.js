const mongoose = require('mongoose')
const moment = require('moment');
const date = new Date()

const Guardians = new mongoose.Schema({
    user_id: {
        type: Number,
        required: true
    },
    guardian_id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    mobile: {
        type: String,
        required: true
    },
    relation: {
        type: String,
        required: true
    },
    priority: {
        type: Number,
        required: true
    },
    requeststatus: {
        type: String,
        default: "Not_accepted",
        required: true
    },
    created_at: {
        type: String,
        default: date
    },
    updated_at: {
        type: String,
        default: date
    }
})

module.exports = mongoose.model('guardians', Guardians)