const Mongoose = require("mongoose");

const Issues = new Mongoose.Schema({
    issue_id: {
        type: String,
        required: true
    },
    user_id: {
        type: String,
        required: true
    },
    guardian_id: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
        default: "Not_responded"
    },
    created_at: {
        type: String,
        required: true,
        default: new Date
    },
    updated_at: {
        type: String,
        required: true,
        default: new Date
    }
})
module.exports = Mongoose.model('issue_tracker', Issues)