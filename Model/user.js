const mongoose = require('mongoose');
const moment = require('moment');
const date = new Date()

const user_master = new mongoose.Schema({
    user_id: {
        type: Number,
        default: 1000
    },
    token: {
        type: String,
        default: ""
    },
    name: {
        type: String,
        default: ""
    },
    location: {
        type: Array,
        required: true
    },
    mobile: {
        type: Number,
        createIndexes: true,
        required: true
    },
    tempmobile: {
        type: Number,
        createIndexes: true,
        required: true
    },
    email: {
        type: String,
        default: ""
    },
    password: {
        type: String,
        default: ""
    },
    DOB: {
        type: String,
        default: ""
    },
    profession: {
        type: String,
        default: ""
    },
    status: {
        type: String,
        default: "Not_verified"
    },
    otp: {
        type: String,
        default: ""
    },
    votes: {
        type: Number,
        default: 0
    },
    is_volenter_also: {
        type: Boolean,
        default: 0
    },
    updated_at: {
        type: Date,
        default: date
    },
    created_at: {
        type: Date,
        default: date
    }
});

module.exports = mongoose.model('User_master', user_master);
