const express = require('express')
const app = express()
const dotenv = require('dotenv')
const mongoose = require('mongoose');
const apiRoute = require('./Router/api');


app.use(express.json())
app.use(express.static('public'))
dotenv.config();

app.listen(process.env.PORT, (err) => {
    if (err) console.log(err)
    console.log("I'm Listening from " + process.env.PORT)
})

mongoose.connect("mongodb://localhost/Guardian", { useNewUrlParser: true, useUnifiedTopology: true }, (err, info) => {
    if (err) console.log(err)
    else console.log("Connected to DB")
})

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Methods', 'POST,PUT,GET,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    next()
})
app.use('/api/', apiRoute)